package es.unex.asee.frojomar.asee_ses.repository.room.roomdb;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import es.unex.asee.frojomar.asee_ses.model.Center;

@Dao
public interface CentersDAO {

    @Query("SELECT * FROM Centers")
    public LiveData<List<Center>> getAll();

    @Query("SELECT * FROM Centers WHERE cityId=:cityId")
    LiveData<List<Center>> getByCity(Integer cityId);

    @Query("SELECT * FROM Centers WHERE id=:id")
    public LiveData<Center> getById(Integer id);

    @Query("SELECT * FROM Centers LIMIT 1")
    public List<Center> existData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Center item);

    @Update
    public void update(Center item);

    @Query("DELETE FROM Centers")
    public void deleteAll();




}
